package ru.t1.akolobov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.event.ConsoleEvent;
import ru.t1.akolobov.tm.listener.AbstractListener;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String DESCRIPTION = "Display available arguments to run application.";

    @NotNull
    @Override
    public String getEventName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@argumentListListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[ARGUMENTS]");
        context.getBeanProvider(AbstractListener.class, false).stream()
                .filter(listener -> listener.getArgument() != null)
                .forEach(listener -> System.out.println(listener.getArgument()));
    }

}
