package ru.t1.akolobov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.request.DataXmlLoadJaxBRequest;
import ru.t1.akolobov.tm.event.ConsoleEvent;

@Component
public final class DataXmlLoadJaxBListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file.";

    @NotNull
    @Override
    public String getEventName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlLoadJaxBListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA LOAD XML]");
        getDomainEndpoint().loadDataXmlJaxb(new DataXmlLoadJaxBRequest(getToken()));
    }

}
