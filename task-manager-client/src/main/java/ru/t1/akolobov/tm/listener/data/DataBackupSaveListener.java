package ru.t1.akolobov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.request.DataBackupSaveRequest;
import ru.t1.akolobov.tm.event.ConsoleEvent;

@Component
public final class DataBackupSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-backup";

    @NotNull
    public static final String DESCRIPTION = "Save backup data to base64 file.";

    @NotNull
    @Override
    public String getEventName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@dataBackupSaveListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        getDomainEndpoint().saveBackup(new DataBackupSaveRequest(getToken()));
    }

}
