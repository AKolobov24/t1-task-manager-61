package ru.t1.akolobov.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.akolobov.tm.dto.model.AbstractDtoModel;

@NoRepositoryBean
public interface CommonDtoRepository<M extends AbstractDtoModel> extends JpaRepository<M, String> {

}
