package ru.t1.akolobov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.dto.model.TaskDto;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskDtoRepository extends UserOwnedDtoRepository<TaskDto> {

    @NotNull
    List<TaskDto> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
