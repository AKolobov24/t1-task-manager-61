package ru.t1.akolobov.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.akolobov.tm.api.service.dto.IProjectDtoService;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.NameEmptyException;
import ru.t1.akolobov.tm.exception.field.StatusEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.repository.dto.ProjectDtoRepository;

@Service
@AllArgsConstructor
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDto> implements IProjectDtoService {

    @NotNull
    @Autowired
    private ProjectDtoRepository repository;

    @NotNull
    @Override
    @Transactional
    public ProjectDto changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final ProjectDto project = repository.findByUserIdAndId(userId, id)
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDto project = new ProjectDto(name);
        add(userId, project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        @NotNull final ProjectDto project = new ProjectDto(name, description);
        add(userId, project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (status == null) return create(userId, name);
        @NotNull final ProjectDto project = new ProjectDto(name, status);
        add(userId, project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDto project = repository.findByUserIdAndId(userId, id)
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

}