package ru.t1.akolobov.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.akolobov.tm.api.service.model.ISessionService;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.exception.entity.SessionNotFoundException;
import ru.t1.akolobov.tm.exception.field.DateEmptyException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.exception.user.RoleEmptyException;
import ru.t1.akolobov.tm.model.Session;
import ru.t1.akolobov.tm.repository.model.SessionRepository;

import java.util.Date;

@Service
@AllArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

    @NotNull
    @Autowired
    private SessionRepository repository;

    @NotNull
    @Override
    @Transactional
    public Session updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Date date,
            @Nullable final Role role
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (date == null) throw new DateEmptyException();
        @NotNull final Session session = repository.findByUserIdAndId(userId, id)
                .orElseThrow(SessionNotFoundException::new);
        session.setDate(date);
        session.setRole(role);
        repository.save(session);
        return session;
    }

}
