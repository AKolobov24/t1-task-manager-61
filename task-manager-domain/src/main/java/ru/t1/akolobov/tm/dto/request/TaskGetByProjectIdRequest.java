package ru.t1.akolobov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskGetByProjectIdRequest(@Nullable final String token) {
        super(token);
    }

}
