package ru.t1.akolobov.tm.logger.api;

import org.jetbrains.annotations.NotNull;

public interface IConsumerService {

    void subscribe(@NotNull final String queue);

}
